import httpretty
import json

from src.services.ghibli import Ghibli


@httpretty.activate
def test_ghlibli_movies(ghibli: Ghibli):
    httpretty.register_uri(
        httpretty.GET,
        "https://localhost.server/films",
        body='[{"sds": "sdsd"}]'
    )
    httpretty.register_uri(
        httpretty.GET,
        "https://localhost.server/people",
        body='[{"sds": "sdsd"}]'
    )

    films = ghibli.films()
    people = ghibli.people()

    assert type(films) is list
    assert json.dumps(films) == '[{"sds": "sdsd"}]'
    assert type(people) is list
    assert json.dumps(people) == '[{"sds": "sdsd"}]'


@httpretty.activate
def test_ghlibli_people(ghibli: Ghibli):
    httpretty.register_uri(
        httpretty.GET,
        "https://localhost.server/films",
        status=404
    )
    try:
        ghibli.films()
    except Exception as e:
        assert str(e) == "404"
    else:
        assert False
