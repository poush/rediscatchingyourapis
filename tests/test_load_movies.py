import httpretty

from src.app.tasks.load_movies import add_person_to_movie, load_movies_people


def test_add_person_to_movie():
    movies = {
        '123': {}
    }
    person = {
        "id": "321"
    }
    add_person_to_movie(person, movies, '123')

    assert type(movies.get('123').get('people')) is list
    assert "321" in movies.get('123').get('people')
    assert "123" not in movies.get('123').get('people')


@httpretty.activate
def test_load_movies_people(redis):

    httpretty.register_uri(
        httpretty.GET,
        "https://localhost.server/films",
        body='[{ "id": "123" }]'
    )

    httpretty.register_uri(
        httpretty.GET,
        "https://localhost.server/people",
        body='[{ "id": "321", "films": ["blahblah/123"] }]'
    )

    load_movies_people("https://localhost.server")

    assert redis.get("movies") is not None
    assert redis.get("people") is not None
