import pytest

from src.app import app as api_app, redis as redisClient
from src.services.ghibli import Ghibli


@pytest.fixture
def app():
    redisClient.delete("movies")
    redisClient.delete("people")
    redisClient.delete("lastSyncedAt")
    redisClient.delete("syncRunning")
    yield api_app


@pytest.fixture
def redis():
    return redisClient


@pytest.fixture
def client(app):
    """A test client for the app."""
    return app.test_client()


@pytest.fixture
def ghibli():
    return Ghibli(api_resource="https://localhost.server")
