# Ghibli Loader

## Requirements

1. You need redis to be installed
2. Update it's endpoint on .env file is hosted somewhere else than localhost
3. `pipenv install`

## Run

1. activate your pipenv shell
2. run celery from project directory: `celery -A src.app.celery worker`
3. Open another terminal shell and run `flask run`

##Observe

1. open `localhost:5000` to observe the frontend
2. `localhost:5000/movies` is an internal API endpoint to return the data

NOTE: for this assignment, I am keeping the `.env` file so that it will be easy fo you to test
Normally, and obviously it will not be in production scenarios.
