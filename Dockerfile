FROM python:3.8.5-slim-buster

LABEL author="Piyush Agrawal<me@ipiyush.com>"


ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

RUN pip install pipenv
RUN apt-get update && apt-get install -y --no-install-recommends gcc

WORKDIR /redisCachingYourAPIs

COPY . .

RUN pipenv lock --requirements > requirements.txt
RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["flask", "run", "--host=0.0.0.0"]