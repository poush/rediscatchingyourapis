import json
import requests


class Ghibli:

    def __init__(self, api_resource):
        self.api_resource = api_resource

    def films(self) -> list:
        data = self._request("films")
        """returns a list of movies from the API"""
        try:
            return json.loads(data)
        except Exception as e:
            print("[movies] invalid json recived", e)
            return []

    def people(self) -> list:
        """ returns a list of people from the API"""
        data = self._request("people")
        try:
            return json.loads(data)
        except Exception as e:
            print("[people] invalid json recived", e)
            return []

    def _request(self, path):
        response = requests.get("{}/{}".format(self.api_resource, path))

        if response.status_code != 200:
            raise Exception(response.status_code)
        return response.text
