import json
import time

from src.services.ghibli import Ghibli
from src.app import celery, redis


def add_person_to_movie(person: dict, movies: dict, movie_id: str):
    if movie_id in movies and person.get("id"):
        if not movies[movie_id].get("people") or not \
                isinstance(movies[movie_id].get("people"), list):
            movies[movie_id]["people"] = []

        movies[movie_id]["people"].append(person["id"])


@celery.task()
def load_movies_people(ghibli_resource):
    # lock sync
    redis.set("syncRunning", 1)
    g = Ghibli(ghibli_resource)
    movies = g.films()
    people = g.people()
    movies_by_id = {}
    for movie in movies:
        _id = movie.get('id')
        if _id is not None:
            # data is not blank
            movies_by_id[_id] = movie

    for person in people:
        if isinstance(person.get("films"), list):
            for person_film in person.get("films"):
                movie_id = person_film.split("/")[-1]
                add_person_to_movie(person, movies_by_id, movie_id)
        else:
            movie_id = person.films.split("/")[-1]
            add_person_to_movie(person, movies_by_id, movie_id)

    redis.set("movies", json.dumps(movies_by_id))
    redis.set("people", json.dumps(people))
    # unlock sync
    redis.set("syncRunning", 0)
    redis.set("lastSyncedAt", int(time.time()))
