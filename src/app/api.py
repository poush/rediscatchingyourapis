from flask import Blueprint, render_template
from src.app.datasources.movies import fetch_movies, fetch_people

apiV1 = Blueprint("v1", __name__)


@apiV1.route('/movies')
def movies():
    movies = fetch_movies()
    people_list = fetch_people()
    return {
        "movies": movies,
        "people": people_list
    }


@apiV1.route('/')
def index():
    return render_template('layout.html')
