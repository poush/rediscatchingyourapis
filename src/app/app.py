import redis
import os

from celery import Celery
from flask import Flask


app = Flask(__name__)
app.config["result_backend"] = os.getenv("REDIS", 'redis://redis:6379')
app.config["CELERY_broker_url"] = os.getenv("REDIS", 'redis://redis:6379')
app.config["GHIBLI_URL"] = os.getenv(
    "GHIBLI_ENDPOINT", "https://ghibliapi.herokuapp.com")

redis = redis.Redis.from_url(os.getenv("REDIS", 'redis://redis:6379'))


def make_celery(app):
    celery = Celery(
        app.import_name,
        broker=app.config['CELERY_broker_url']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


celery = make_celery(app)
