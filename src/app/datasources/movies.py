import time
import json

from src.app.tasks.load_movies import load_movies_people
from src.app.app import redis, app


def refresh_expired_data(func):
    def wrapper():
        diff = int(time.time()) - int(redis.get("lastSyncedAt") or 0)
        if diff > int(58) and int(redis.get("syncRunning") or 0) == 0:
            load_movies_people.delay(app.config["GHIBLI_URL"])
        return func()
    return wrapper


def fetch_movies():
    return json.loads(fetch_movies_cached() or "[]")


@refresh_expired_data
def fetch_movies_cached() -> str:
    return redis.get("movies")


def fetch_people():
    return json.loads(fetch_people_cached() or "[]")


def fetch_people_cached() -> str:
    # we don't need to check for expired data twice
    # as if movies are expired then it will update people also
    return redis.get("people")
