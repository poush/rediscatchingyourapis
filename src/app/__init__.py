from .app import app, celery, redis  # noqa
from .api import apiV1

app.register_blueprint(apiV1)
